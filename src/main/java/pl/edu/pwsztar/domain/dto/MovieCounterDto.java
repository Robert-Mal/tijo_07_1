package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private long counter;

    public MovieCounterDto(long counter) {
        this.counter = counter;
    }

    public MovieCounterDto(Builder builder){
        counter = builder.counter;
    }

    public long getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }

    public static final class Builder {
        private long counter;

        public Builder() {}

        public Builder counter(long counter) {
            this.counter = counter;
            return this;
        }
    }
}
